window.addEventListener('click', function (event) {
    console.log(event);
});

let chara1 = document.querySelector('.perso1');
let chara2 = document.querySelector('.perso2');
let infoP1 = document.querySelector('.infoP1');
let infoP2 = document.querySelector('.infoP2');


perso1 = {

    pa: 20,
    pm: 10,
}

perso2 = {

    pa: 20,
    pm: 10,
}

let pHealth = document.querySelector('#pHealth');
let pPa = document.querySelector('.pPa');
let pPm = document.querySelector('.pPm');
let pHealth2 = document.querySelector('#pHealth2');
let pPa2 = document.querySelector('.pPa2');
let pPm2 = document.querySelector('.pPm2');


pPa.textContent = `PA = ${perso1.pa}`;
pPm.textContent = `PM = ${perso1.pm}`;



pPa2.textContent = `PA = ${perso2.pa}`;
pPm2.textContent = `PM = ${perso2.pm}`;


if (pHealth2.value > 0 && pHealth.value > 0) {

    document.querySelector('body').addEventListener('keydown', function (event) {
        console.log(event.code);

        let chara1 = document.querySelector('.perso1');
        let pHealth = document.querySelector('#pHealth');
        let pHealth2 = document.querySelector('#pHealth2');
        let pPa = document.querySelector('.pPa');
        let pPm = document.querySelector('.pPm');


        if (event.code === 'KeyQ' && perso1.pa > 0 && pHealth.value > 0 && pHealth2.value > 0) {
            move(chara1, 700);
            setTimeout(function () { moveReturn(chara1, 0) }, 1000);
            pHealth2.value -= 20;
            perso1.pa = perso1.pa - 1;
            pPa.textContent = `PA = ${perso1.pa}`;


            setTimeout(ennemyFight, 1200);
        } else if (event.code === 'KeyW' && perso1.pm > 0 && pHealth.value > 0 && pHealth2.value > 0) {
            move(chara1, 700);
            setTimeout(function () { moveReturn(chara1, 0) }, 1000);
            pHealth2.value -= Math.floor((Math.random() * 30) + 10);
            perso1.pm = perso1.pm - 1;
            pPm.textContent = `PM = ${perso1.pm}`;
            setTimeout(ennemyFight, 1200);

        } else if (event.code === 'KeyS' && perso1.pm > 0 && pHealth.value > 0 && pHealth2.value > 0) {

            pHealth.value += Math.floor((Math.random() * 20) + 10);
            perso1.pm = perso1.pm - 1;
            pPm.textContent = `PM = ${perso1.pm}`;
            setTimeout(ennemyFight, 1200);

        }
        else if (event.code === 'KeyA' && pHealth.value > 0 && pHealth2.value > 0) {
            perso1.pm += Math.floor((Math.random() * 5) + 1);
            pPm.textContent = `PM = ${perso1.pm}`;
            setTimeout(ennemyFight, 1200);

        }
        winOrLose();

    });

}





function ennemyFight() {  //fonction pour faire attaquer l'ennemi de manière aléatoire
    let ennemyAttack = [`attack`, `object`, `sort`, `heal`];
    let chara2 = document.querySelector('.perso2');
    let pHealth = document.querySelector('#pHealth');
    let pHealth2 = document.querySelector('#pHealth2');
    let pPa2 = document.querySelector('.pPa2');
    let pPm2 = document.querySelector('.pPm2');

    if (perso2.pm === 0) {
        ennemyAttack = [`attack`, `object`];
    } else if (perso2.pa === 0) {
        ennemyAttack = [`object`, `sort`, `heal`];
    }
    let randomAttack = ennemyAttack[Math.floor(Math.random() * ennemyAttack.length)];
    console.log(randomAttack);


    if (randomAttack === 'attack' && perso2.pa > 0 && pHealth.value > 0 && pHealth2.value > 0) {
        move(chara2, 500, `./image/thanosattack.gif`);
        setTimeout(function () { moveReturn(chara2, 1000) }, 1000);
        pHealth.value -= 20;
        perso2.pa = perso2.pa - 1;
        pPa2.textContent = `PA = ${perso2.pa}`;

        console.log('ok2 ' + pHealth.value);
    } else if (randomAttack === `sort` && perso2.pm > 0 && pHealth.value > 0 && pHealth2.value > 0) {
        move(chara2, 360, `./image/thanosattack2.gif`);
        setTimeout(function () { moveReturn(chara2, 1000) }, 1000);
        pHealth.value -= Math.floor((Math.random() * 30) + 10);
        perso2.pm = perso2.pm - 1;
        pPm2.textContent = `PM = ${perso2.pm}`;

    } else if (randomAttack === `heal` && perso2.pm > 0 && pHealth.value > 0 && pHealth2.value > 0) {

        pHealth2.value += Math.floor((Math.random() * 20) + 10);
        perso2.pm = perso2.pm - 1;
        pPm2.textContent = `PM = ${perso2.pm}`;
    } else if (randomAttack === `object` && pHealth.value > 0 && pHealth2.value > 0) {
        perso2.pm += Math.floor((Math.random() * 5) + 1);
        pPm2.textContent = `PM = ${perso2.pm}`;
    }
    winOrLose();
}



/**
 * 
 * @param {variable} perso 
 * @param {number} position 
 */
function move(perso, position, source) {  //fonction pour faire "l'animation" des attaques
    let imgP1 = document.querySelector('.imgP1');
    let imgP2 = document.querySelector('.imgP2')
    perso.style.transitionDuration = '0.5s';
    perso.style.left = position + 'px';
    if (perso === chara1 && event.code === 'KeyQ') {
        imgP1.src = './image/cloudattack2.gif';
    } else if (perso === chara1 && event.code === 'KeyW') {
        imgP1.src = './image/cloudattack.gif';
    }
    if (perso === chara2) {
        imgP2.src = source;
    }
    if (perso === chara2) {
        imgP2.src = source;
    }


}

/**
 * 
 * @param {variable} perso 
 * @param {number} position 
 */
function moveReturn(perso, position, ) {  //fonction pour faire revenir le personnage à sa position
    let imgP1 = document.querySelector('.imgP1');
    let imgP2 = document.querySelector('.imgP2')
    perso.style.transitionDuration = '0.5s';
    perso.style.left = position + 'px';
    if (perso === chara1) {
        imgP1.src = './image/cloudpose.png';
    }
    if (perso === chara2) {
        imgP2.src = './image/thanospose.png';
    }
}



function winOrLose() {   //fonction pour afficher le message you win ou you lose

    let pHealth2 = document.querySelector('#pHealth2');
    let pHealth = document.querySelector('#pHealth');
    if (pHealth2.value <= 0) {

        let mess = document.querySelector('.mess');
        let winLose = document.querySelector('.winLose')
        winLose.textContent = 'You Win';
        mess.style.display = 'block';
    } else if (pHealth.value <= 0) {

        let mess = document.querySelector('.mess');
        let winLose = document.querySelector('.winLose')
        winLose.style.display = 'block';
        mess.textContent = 'You Lose';
    }
}
