window.addEventListener('click', function (event) {
    console.log(event);
});

let chara = document.querySelector('.personnage');

let charaData = {
    x: 918,
    y: 558
};


moveChara(charaData);

document.querySelector('body').addEventListener('keydown', function (event) {
   

    if (event.code === 'ArrowRight' && charaData.x <= 918) {
        charaData.x += 48;
    }
    else if (event.code === 'ArrowLeft' && charaData.x >= 623) {
        charaData.x -= 48;
    }
    else if (event.code === 'ArrowUp' && charaData.y >= 229) {
        charaData.y -= 48;
    }
    else if (event.code === 'ArrowDown' && charaData.y <= 543) {
        charaData.y += 48;
    }

    moveChara(charaData);
    collision();


});



function moveChara(chara) { //fonction pour déplacer le personnage

    let div = document.querySelector('.personnage');

    div.style.left = chara.x + 'px';
    div.style.top = chara.y + 'px';

}

function collision() {  //fonction pour les collisions entre le personnage et la sortie
    let chara = document.querySelector('.personnage');
    let sortie = document.querySelector('.sortie');

    if (chara.offsetLeft < sortie.offsetLeft + sortie.offsetWidth &&
        chara.offsetLeft + chara.offsetWidth > sortie.offsetLeft &&
        chara.offsetTop < sortie.offsetTop + sortie.offsetHeight &&
        chara.offsetHeight + chara.offsetTop > sortie.offsetTop) {

        history.go(-1);
    }

}