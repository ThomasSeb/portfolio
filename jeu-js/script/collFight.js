
/**
 * 
 * @param {variable} ennemi 
 */
function collisionFight(ennemi) {   //fonction pour la collision avec un ennemi

    let chara = document.querySelector('.personnage');
    
        if (chara.offsetLeft < ennemi.offsetLeft + ennemi.offsetWidth &&
            chara.offsetLeft + chara.offsetWidth > ennemi.offsetLeft &&
            chara.offsetTop < ennemi.offsetTop + ennemi.offsetHeight &&
            chara.offsetHeight + chara.offsetTop > ennemi.offsetTop) {


                if (ennemi === ennemiMacdo) {
            window.location.href="https://www.simplonlyon.fr/promo9/tsebastianelli/jeu-js/fightMacdo.html"
                    
                }else if (ennemi === ennemiThanos) {
            window.location.href="https://www.simplonlyon.fr/promo9/tsebastianelli/jeu-js/fightThanos.html"
                    
                }
        }

}


function collisionForet(lieu, pageHtml) {   //fonction pour la collision avec l'arbre

    let chara = document.querySelector('.personnage');
    let foret = document.querySelector('.arbre');
    
        if (chara.offsetLeft < lieu.offsetLeft + lieu.offsetWidth &&
            chara.offsetLeft + chara.offsetWidth > lieu.offsetLeft &&
            chara.offsetTop < lieu.offsetTop + lieu.offsetHeight &&
            chara.offsetHeight + chara.offsetTop > lieu.offsetTop) {


                
            window.location.href=pageHtml;
                    
             
        }

}

