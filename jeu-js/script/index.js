let chara = document.querySelector('.personnage');
let maison1 = document.querySelectorAll('.collision1');
let buisson = document.querySelector('.buisson');
let ennemiMacdo = document.querySelector('.ennemiMacdo');
let ennemiThanos = document.querySelector('.ennemiThanos');


window.addEventListener('click', function (event) {
    console.log(event);
});


//emplacement et mouvement du perso sur la map


let charaData = {
    x: 624,
    y: 555
};



moveChara(charaData);


document.querySelector('body').addEventListener('keydown', function (event) {
    if (event.code === 'ArrowRight' && charaData.x <= 1235 && badColl(charaData.x + 68, charaData.y) === true) {

        charaData.x += 68;
    }
    else if (event.code === 'ArrowLeft' && charaData.x >= 75 && badColl(charaData.x - 68, charaData.y) === true) {
        charaData.x -= 68;
    }
    else if (event.code === 'ArrowUp' && charaData.y >= 75 && badColl(charaData.x, charaData.y - 68) === true) {
        charaData.y -= 68;
    }
    else if (event.code === 'ArrowDown' && charaData.y <= 500 && badColl(charaData.x, charaData.y + 68) === true) {
        charaData.y += 68;
    }

    let foret = document.querySelector('.arbre');
    let chateau = document.querySelector('.chateau');
    moveChara(charaData);
    collisionFight(ennemiMacdo);
    collisionFight(ennemiThanos);
    collisionMaison();
    collisionForet(foret, "https://www.simplonlyon.fr/promo9/tsebastianelli/jeu-js/foret.html");
    collisionForet(chateau, "https://www.simplonlyon.fr/promo9/tsebastianelli/jeu-js/chateau.html");
    


});


/**
 * 
 * @param {object} chara 
 */
function moveChara(chara) {  // fonction pour faire bouger le personnage

    let div = document.querySelector('.personnage');

    div.style.left = chara.x + 'px';
    div.style.top = chara.y + 'px';

}




function badColl(x, y) {
    let chara = document.querySelector('.personnage');
    let buisson = document.querySelectorAll('.collBuiss');

    for (const item of buisson) {


        if (x < item.offsetLeft + item.offsetWidth &&
            x + chara.offsetWidth > item.offsetLeft &&
            y < item.offsetTop + item.offsetHeight &&
            chara.offsetHeight + y > item.offsetTop) {

            return false;


        } 
    }return true;
}

